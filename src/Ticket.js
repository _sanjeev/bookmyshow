const allMovies = [
    {
        id: 1,
        details: [{
            "showid":1,
            "theaterName": "Citylight Cinema : Mahim",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":2,
            "theaterName": "INFOX: Korum Mall Eastern Express",
            "showTime": ["10:15pm  ", "12:20pm  ", "01:15pm  ", "04:15pm  ", "06:15pm  ", "07:15pm  "]
        },
        {
            "showid":3,
            "theaterName": "PVR",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":4,
            "theaterName": "Bharat Complex",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":5,
            "theaterName": "Cinepolis",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":6,
            "theaterName": "Gold Cinema",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":7,
            "theaterName": "Gopi Cinema",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":8,
            "theaterName": "Carnival",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":9,
            "theaterName": "The Cinema GT World Mall",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        }]
    },
    {
        id: 2,
        details: [{
            "showid":1,
            "theaterName": "Citylight Cinema : Mahim",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":2,
            "theaterName": "INFOX: Korum Mall Eastern Express",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":3,
            "theaterName": "PVR",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":4,
            "theaterName": "Bharat Complex",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":5,
            "theaterName": "Cinepolis",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":6,
            "theaterName": "Gold Cinema",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":7,
            "theaterName": "Gopi Cinema",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":8,
            "theaterName": "Carnival",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":9,
            "theaterName": "The Cinema GT World Mall",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        }]
    },
    {
        id: 3,
        details: [{
            "showid":1,
            "theaterName": "Citylight Cinema : Mahim",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":2,
            "theaterName": "INFOX: Korum Mall Eastern Express",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":3,
            "theaterName": "PVR",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":4,
            "theaterName": "Bharat Complex",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":5,
            "theaterName": "Cinepolis",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":6,
            "theaterName": "Gold Cinema",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":7,
            "theaterName": "Gopi Cinema",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":8,
            "theaterName": "Carnival",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":9,
            "theaterName": "The Cinema GT World Mall",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        }]
    },
    {
        id: 4,
        details: [{
            "showid":1,
            "theaterName": "Citylight Cinema : Mahim",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":2,
            "theaterName": "INFOX: Korum Mall Eastern Express",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":3,
            "theaterName": "PVR",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":4,
            "theaterName": "Bharat Complex",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":5,
            "theaterName": "Cinepolis",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":6,
            "theaterName": "Gold Cinema",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":7,
            "theaterName": "Gopi Cinema",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":8,
            "theaterName": "Carnival",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":9,
            "theaterName": "The Cinema GT World Mall",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        }]
    },
    {
        id: 5,
        details: [{
            "showid":1,
            "theaterName": "Citylight Cinema : Mahim",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":2,
            "theaterName": "INFOX: Korum Mall Eastern Express",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":3,
            "theaterName": "PVR",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":4,
            "theaterName": "Bharat Complex",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":5,
            "theaterName": "Cinepolis",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":6,
            "theaterName": "Gold Cinema",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":7,
            "theaterName": "Gopi Cinema",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":8,
            "theaterName": "Carnival",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        },
        {
            "showid":9,
            "theaterName": "The Cinema GT World Mall",
            "showTime": ["10:15pm", "12:20pm", "01:15pm", "04:15pm", "06:15pm", "07:15pm"]
        }]
    }
    
]

export default allMovies;
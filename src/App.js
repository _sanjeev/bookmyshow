import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom/cjs/react-router-dom.min';
import Home from './components/Home';
import './App.css';
import Main from './components/Main';
import MovieDetails from './components/MovieDetails';
import Footer from './components/Footer';
import AllMovies from './components/AllMovies';
import AllMovieDetails from './components/AllMovieDetails';
import Navbar from './components/Navbar';
import Signup from './components/Signup';
import Login from './components/Login';
import Ticket from './components/Ticket';
import SeatBooking from './components/SeatBooking';
import Success from './components/Success';

export default class App extends Component {
  render() {
    return (
      <Router>
        <div className=' App'>
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route exact path='/details/:id' component={MovieDetails} />
            <Route exact path='/allmovies/:id' component={AllMovieDetails} />
            <Route exact path="/movies" component={AllMovies}/>
            <Route exact path="/signup" component={Signup} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/tickets/:id" component={Ticket} />
            <Route exact path="/seatbooking/:id" component={SeatBooking} />
            <Route exact path="/successfullybookticket" component={Success} />
          </Switch>
        </div>
      </Router>
    )
  }
}

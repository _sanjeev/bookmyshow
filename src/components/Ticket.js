import React, { Component } from 'react';
import Header from './Header';
import Navbar from './Navbar';
import allData from '../AllData';
import date from '../date';
import './ticket.css';
import Timing from './Timing';
export default class Ticket extends Component {
    render() {
        const id = this.props.match.params.id;
        console.log(allData[id - 1]);
        return (
            <div>
                <Navbar />
                <Header />
                <div className='ticket-header'>
                    <div className='container'>
                        <div className='row'>
                            <div className='col-6'>
                                <h3 className='text-start py-2'>{allData[id - 1].movie_name}</h3>
                                <div className='d-flex align-items-center py-2'>
                                    <span><i class="fas fa-heart heart"></i></span>
                                    <h3 className='text-start px-2'>{allData[id - 1].ratings} <span className='comedy-type border p-1 mb-3'>{allData[id - 1].movie_type}</span> <span className='comedy-type border p-1 mb-3'>{allData[id - 1].date}</span></h3>

                                </div>
                            </div>
                            <div className='col-6'>
                                <div className='d-flex justify-content-end'>
                                    <div className='d-flex flex-column align-items-start'>
                                        <p>{allData[id - 1].crew_type[0]}</p>
                                        <img src={allData[id - 1].crew[0]} className='casting' />
                                        <p>Aditya...</p>
                                    </div>
                                    <div className='d-flex flex-column align-items-start px-3'>
                                        <p>{allData[id - 1].cast_type[0]}</p>
                                        <img src={allData[id - 1].cast[0]} className='casting' />
                                        <p>Amey...</p>
                                    </div>
                                    <div className='d-flex flex-column align-items-start px-3'>
                                        <p>{allData[id - 1].cast_type[1]}</p>
                                        <img src={allData[id - 1].cast[1]} className='casting' />
                                        <p>Vaidehi...</p>
                                    </div>
                                    <div className='d-flex flex-column align-items-start px-3'>
                                        <p>{allData[id - 1].cast_type[2]}</p>
                                        <img src={allData[id - 1].cast[2]} className='casting' />
                                        <p>Lalit...</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='bg-white'>
                    <div className='container'>
                        <div className='row'>
                            <div className='col-4'>
                                <div className='d-flex'>
                                    {date.map((key) => (
                                        <div>
                                            <p>{key.date}</p>
                                            <p>{key.day}</p>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>
                    </div>
                    <Timing id={id}/>
                </div>
            </div>
        )
    }
}

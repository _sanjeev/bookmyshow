import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Login.css';
export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: {
                value: "",
                flag: false,
            },
            password: {
                value: "",
                flag: false,
            },
            modal: false,
        };
    }

    handleOnChange = (event) => {
        if (event.target.name === "email") {
            this.setState({
                email: {
                    value: event.target.value,
                    flag: true,
                },
                modal: false,
            });
        }

        if (event.target.name === "password") {
            this.setState({
                password: {
                    value: event.target.value,
                    flag: true,
                },
                modal: false,
            });
        }
    };

    onSubmitForm = (event) => {
        event.preventDefault();
        console.log("submit event");

        if (
            (this.state.email.flag === true) &
            (this.state.password.flag === true) &
            (localStorage.getItem('email') === this.state.email.value) &
            (this.state.password.value === localStorage.getItem("password"))
        ) {
            this.setState({
                modal: true,
            });
        } else {
            alert("invalid userid and password");
        }
    };
    render() {
        return (
            <div className='body'>
                <div className='container'>
                    <div className='row justify-content-center p-5'>
                        <div className='col-5 shadow-lg p-5 mb-5 bg-white rounded'>
                            <div>
                                <div>
                                    <h1>Login</h1>
                                </div>
                                <div className='d-flex flex-column mt-3'>
                                    <div>
                                        <label>Email</label>
                                    </div>
                                    <div>
                                        <input type='email' className='w-100'
                                            required
                                            placeholder='Enter Your Email'
                                            name="email"
                                            defaultValue={this.state.email.value}
                                            onChange={this.handleOnChange}
                                            auto
                                            Complete="off"
                                        />
                                        <div className="valid-data">{this.state.email.valid}</div>
                                        <div className="invalid-data">{this.state.email.inValid}</div>
                                    </div>
                                </div>
                                <div className='d-flex flex-column mt-3'>
                                    <div>
                                        <label>Password</label>
                                    </div>
                                    <div>
                                        <input type='password' className='w-100'
                                            required
                                            placeholder='Enter your Password'
                                            name="password"
                                            defaultValue={this.state.password.value}
                                            onChange={this.handleOnChange}
                                        />
                                        <div className="valid-data">{this.state.password.valid}</div>
                                        <div className="invalid-data">
                                            {this.state.password.inValid}
                                        </div>
                                    </div>
                                </div>
                                <div className='mt-3'>
                                    {
                                        this.state.modal === false ? <button className='btn mt-3 text-white' type='submit' onClick={this.onSubmitForm}>Login</button> : <><Link to="/">Go to Home</Link></>
                                    }
                                </div>
                                <div className='d-flex justify-content-center mt-3 font-color'>
                                    Forgot your Password?
                                </div>
                                <div className='d-flex justify-content-center mt-3'>
                                    or
                                </div>
                                <div className='d-flex justify-content-center mt-1'>
                                    <button className='d-flex justify-content-center align-items-center w-100 signin border'>
                                        <div>
                                            <img src='https://assets.splitwise.com/assets/fat_rabbit/signup/google-2017-a5b76a1c1eebd99689b571954b1ed40e13338b8a08d6649ffc5ca2ea1bfcb953.png' alt="Google" />
                                        </div>
                                        <div className='mt-3 mx-1'>
                                            <p>SignUp With Google</p>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}






































// import React from "react";
// import { Link } from "react-router-dom";
// import './signup.css';

// class Login extends React.Component {
//   // eslint-disable-next-line
//   constructor(props) {
//     super(props);
//     this.state = {
//       email: {
//         value: "",
//         flag: false,
//       },
//       password: {
//         value: "",
//         flag: false,
//       },
//       modal: false,
//     };
//   }

//   handleOnChange = (event) => {
//     if (event.target.name === "email") {
//       this.setState({
//         email: {
//           value: event.target.value,
//           flag: true,
//         },
//         modal: false,
//       });
//     }

//     if (event.target.name === "password") {
//       this.setState({
//         password: {
//           value: event.target.value,
//           flag: true,
//         },
//         modal: false,
//       });
//     }
//   };

//   onSubmitForm = (event) => {
//     event.preventDefault();
//     console.log("submit event");

//     if (
//       (this.state.email.flag === true) &
//       (this.state.password.flag === true) &
//       (localStorage.getItem('email') === this.state.email.value) &
//       (this.state.password.value === localStorage.getItem("password"))
//     ) {
//       this.setState({
//         modal: true,
//       });
//     } else {
//       alert("invalid userid and password");
//     }
//   };

//   render() {
//     console.log(this.props.user);
//     return (
//       <div className="form-container">
//         <div className="container-box">
//           {/* <div className="container">
//             <img src={code} alt="" className="code" />
//           </div> */}
//           <div className="container" style={{ marginTop: "-15px" }}>
//             {/* <LoginModal flag={this.state.modal} />
//             <div className="icon-box">
//               <img src={icon} alt="sign icon" className="icon" />
//             </div> */}
//             <form onSubmit={this.onSubmitForm}>
//               <div className="form-group row ">
//                 <div className="col">
//                   <label>Email</label>
//                   <input
//                     type="text"
//                     className="form-control"
//                     required
//                     placeholder="Enter email"
//                     name="email"
//                     defaultValue={this.state.email.value}
//                     onChange={this.handleOnChange}
//                     autoComplete="off"
//                   />
//                   <div className="valid-data">{this.state.email.valid}</div>
//                   <div className="invalid-data">{this.state.email.inValid}</div>
//                 </div>
//               </div>

//               <div className="form-group row ">
//                 <div className="col">
//                   <label>Password</label>
//                   <input
//                     type="password"
//                     className="form-control"
//                     required
//                     placeholder="Enter password"
//                     name="password"
//                     defaultValue={this.state.password.value}
//                     onChange={this.handleOnChange}
//                   />
//                   <div className="valid-data">{this.state.password.valid}</div>
//                   <div className="invalid-data">
//                     {this.state.password.inValid}
//                   </div>
//                 </div>
//               </div>
//               <div className="form-group row py-3">
//                 <div className="col">
//                   <label>
//                     if Don't have account{" "}
//                     <Link to="/signup" style={{ color: "blueviolet" }}>
//                       Register here
//                     </Link>
//                   </label>
//                 </div>
//               </div>

//               <div className="form-group row align-items-center">
//                 <div class="col-12">
//                   {this.state.modal === false ? <input
//                     class="btn btn-primary"
//                     type="submit"
//                     style={{ width: "100%" }}
//                     value={`Login`}
//                   /> : <><Link to="/">
//                   Go to Home</Link></>}
//                 </div>
//               </div>
//             </form>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }
// export default Login;

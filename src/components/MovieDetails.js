import React, { Component } from 'react';
import res from '../data.js';
import Footer from './Footer.js';
import Header from './Header.js';
import './moviedetails.css';
import MovieDetailsRecommendation from './MovieDetailsRecommendation.js';
import Navbar from './Navbar.js';
import Recommended from './Recommended.js';

export default class MovieDetails extends Component {
    constructor(props) {
        super(props);
        const id = this.props.match.params.id;
    }
    render() {
        const id = this.props.match.params.id;
        const output = res.filter((key) => {
            if (key.id !== Number(id)) {
                console.log(key.id, id);
                return true;
            }
        })
        console.log(res[id - 1]);
        return (
            <div>
                <Navbar />
                <Header />
                <div className='container-fluid movie-details'>
                    <div className='container'>
                        <div className='row p-5'>
                            <div className='col-4 px-0 mx-0'>
                                <div className=' image-movie'><img src={res[id - 1].image} className='movie-details-image rounded' /></div>
                            </div>
                            <div className='col-4 px-3  d-flex align-items-start  flex-column'>
                                <div>
                                    <h1 className='text-white'>{res[id - 1].movie_name}</h1>
                                </div>

                                <div className='d-flex mt-5 justify-content-start align-items-center'>
                                    <div>
                                        <i class="fas fa-heart heart"></i>
                                    </div>
                                    <div>
                                        <h4 className='text-white px-3 mt-1'>{res[id - 1].like}</h4>
                                    </div>
                                    <div>
                                        <p className='text-white mt-3'>{res[id - 1].ratings}ratings</p>
                                    </div>
                                </div>
                                <div className='d-flex mt-5'>
                                    <div>
                                        <button className='btn bg-white'>{res[id - 1].cinema_type}</button>
                                    </div>
                                    <div>
                                        <button className='btn bg-white mx-1'>{res[id - 1].language}</button>
                                    </div>
                                </div>
                                <div className='d-flex mt-5'>
                                    <div>
                                        <p className='text-white'>{res[id - 1].date}</p>
                                    </div>
                                </div>
                                <div className='d-flex mt-5'>
                                    <div>
                                        <button className='book-tickets text-white'>Book Tickets</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='container mt-5'>
                    <div className='row'>
                        <div className='col-12'>
                            <h3 className='text-start'>About the Movies</h3>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-12'>
                            <p className='text-start'>{res[id - 1].about}</p>
                        </div>
                    </div>
                </div>
                <div className='container mt-5'>
                    <div className='row'>
                        <div className='col-12'>
                            <h3 className='text-start'>Cast</h3>
                        </div>
                    </div>
                    <div className='row mt-4'>
                        {res[id - 1].cast.map((key, index) => (
                            <div className='col-3' key={`movie${index}`}>
                                <img src={key} className='w-100 rounded-circle' />
                                <p className='mt-3 cast_name'>{res[id - 1].cast_name[index]}</p>
                                <p>{res[id - 1].cast_type[index]}</p>
                            </div>
                        ))}
                    </div>
                </div>
                <div className='container mt-5'>
                    <div className='row'>
                        <div className='col-12'>
                            <h3 className='text-start'>Crew</h3>
                        </div>
                    </div>
                    <div className='row mt-4'>
                        {res[id - 1].crew.map((key, index) => (
                            <div className='col-3' key={`movie${index}`}>
                                <img src={key} className='w-100 rounded-circle' />
                                <p className='mt-3 cast_name'>{res[id - 1].crew_name[index]}</p>
                                <p>{res[id - 1].crew_type[index]}</p>
                            </div>
                        ))}
                    </div>
                </div>
                <MovieDetailsRecommendation data = {output}/>
                <Footer />
            </div>
        )
    }
}

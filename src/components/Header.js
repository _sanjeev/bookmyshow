import React, { Component } from 'react';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import './header.css';
export default class Header extends Component {
    render() {
        return (
            <div className='list d-flex align-items-center'>
                <div className='container py-5'>
                    <div className='row text-white'>
                        <div className='col-1 d-flex justify-content-center align-items-center'>
                        <Link to="/movies" className="text-decoration-none text-white">
                                Movies
                            </Link>
                        </div>
                        <div className='col-1 '>
                            <div className='p-a'>
                                Streams<div className='super'>NEW</div>
                            </div>
                        </div>
                        <div className='col-1 d-flex justify-content-center align-items-center'>
                            Events
                        </div>
                        <div className='col-1 d-flex justify-content-center align-items-center'>
                            Plays
                        </div>
                        <div className='col-1 d-flex justify-content-center align-items-center'>
                            Sports
                        </div>
                        <div className='col-1 d-flex justify-content-center align-items-center'>
                            Activities
                        </div>
                        <div className='col-1 d-flex justify-content-center align-items-center'>
                            Buzz
                        </div>
                        <div className='col-1 d-flex justify-content-center align-items-center'>

                        </div>
                        <div className='col-1'>

                            <div className='p-a'>
                                ListYourShow<div className='list_super'>NEW</div>
                            </div>
                        </div>
                        <div className='col-1 d-flex justify-content-center align-items-center'>
                            Corporates
                        </div>
                        <div className='col-1 d-flex justify-content-center align-items-center'>
                            Offers
                        </div>
                        <div className='col-1 d-flex justify-content-center align-items-center'>
                            GiftCards
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

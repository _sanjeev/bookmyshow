import React, { Component } from 'react';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import allMovies from '../Ticket';
export default class Timing extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const id = this.props.id;
        console.log(allMovies[this.props.id].details);
        return (
            <div>
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            {allMovies[this.props.id].details.map((key, index) => (
                                <div className='shadow pt-4 p-2 bg-white rounded mb-2 d-flex align-items-center justify-content-between'>
                                    <div>
                                        <p className='text-start'>{key.theaterName}</p>
                                    </div>
                                    <div className='d-flex align-items-center justify-content-center'>
                                        {key.showTime.map((ele, index) => (
                                            <div>
                                                {console.log(key.showid)}
                                                <div style={{ cursor: 'pointer' }} className='mx-2 border pt-3 px-2 d-flex align-items-center justify-content-center'>

                                                    <Link to={{ pathname: `/seatbooking/${index}${key.showid}` }} className="text-decoration-none">
                                                        <p className='text-success'>{ele}</p>
                                                    </Link>
                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

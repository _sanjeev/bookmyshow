import React, { Component } from 'react';
import './trendingsearch.css';

export default class TrendingSearch extends Component {
    render() {
        return (
            <div className='container'>
                <div className='row mt-5'>
                    <div className='col-12'>
                        <h4 className='text-start'>Trending Searches Right Now</h4>
                    </div>
                    <div className='row d-flex'>
                        <div className='col-1'>
                            <div className='d-flex flex-column border justify-content-center align-items-center bg-white px-2 pt-2'>
                                <h6 className='movie-name'>Zombivli</h6>
                                <p className='movie-type'>Movies</p>
                            </div>
                        </div>
                        <div className='col-3'>
                            <div className='d-flex flex-column border justify-content-center align-items-center bg-white px-2 pt-2'>
                                <h6 className='movie-name '>Pushpa: The Rise - Part 01</h6>
                                <p className='movie-type'>Movies</p>
                            </div>
                        </div>
                        <div className='col-1'>
                            <div className='d-flex flex-column border justify-content-center align-items-center bg-white px-2 pt-2'>
                                <h6 className='movie-name'>RRR</h6>
                                <p className='movie-type'>Movies</p>
                            </div>
                        </div>
                        <div className='col-4'>
                            <div className='d-flex flex-column border justify-content-center align-items-center bg-white px-2 pt-2'>
                                <h6 className='movie-name'>Nay Varan Bhat Loncha Kon Nay Koncha</h6>
                                <p className='movie-type'>Movies</p>
                            </div>

                        </div>
                        <div className='col-2 '>
                            <div className='d-flex flex-column border justify-content-center align-items-center bg-white px-2 pt-2'>
                                <h6 className='movie-name'>Scream (2022)</h6>
                                <p className='movie-type'>Movies</p>
                            </div>

                        </div>
                    </div>
                    <div className='row mt-3'>
                        <div className='col-3'>
                            <div className='d-flex flex-column border justify-content-center align-items-center bg-white px-2 pt-2'>
                                <h6 className='movie-name'>Spider-Man: No Way Home </h6>
                                <p className='movie-type'>Movies</p>
                            </div>
                        </div>
                        <div className='col-1'>
                            <div className='d-flex flex-column border justify-content-center align-items-center bg-white px-2 pt-2'>
                                <h6 className='movie-name'>83</h6>
                                <p className='movie-type'>Movies</p>
                            </div>
                        </div>
                        <div className='col-3'>
                            <div className='d-flex flex-column border justify-content-center align-items-center bg-white px-2 pt-2'>
                                <h6 className='movie-name'>Ala Vaikunthapurramullo</h6>
                                <p className='movie-type'>Movies</p>
                            </div>
                        </div>
                        <div className='col-3'>
                            <div className='d-flex flex-column border justify-content-center align-items-center bg-white px-2 pt-2'>
                                <h6 className='movie-name'>K.G.F. Chapter 2</h6>
                                <p className='movie-type'>Movies</p>
                            </div>
                        </div>
                        <div className='col-1'>
                            <div className='d-flex flex-column border justify-content-center align-items-center bg-white px-2 pt-2'>
                                <h6 className='movie-name'>Haridayam</h6>
                                <p className='movie-type'>Movies</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

import React, { Component } from 'react';

export default class OutdoorEvents extends Component {
  render() {
    return (
        <div className='container'>
            <div className='row'>
              <div className='col-12'>
                <h4 className='text-start'>Outdoor Events</h4>
              </div>
            </div>
            <div className='row d-flex justify-content-between align-items-center'>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:oi-discovery-catalog@@icons@@heart_202006300400.png,ox-24,oy-617,ow-29:ote-ODclICAxayB2b3Rlcw%3D%3D,ots-29,otc-FFFFFF,oy-612,ox-70/et00136822-cjealsyuay-portrait.jpg' className='card-image rounded'/>
                <h5>Zombivli</h5>
                <p>Commedy/Horror</p>
              </div>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:oi-discovery-catalog@@icons@@heart_202006300400.png,ox-24,oy-617,ow-29:ote-ODQlICA0NDZrIHZvdGVz,ots-29,otc-FFFFFF,oy-612,ox-70/et00129538-xlannwuhpw-portrait.jpg' className='card-image rounded'/>
                <h5>Pushpa: The Rise</h5>
                <p>Action/Thriller</p>
              </div>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:oi-discovery-catalog@@icons@@heart_202006300400.png,ox-24,oy-617,ow-29:ote-ODklICAxMjVrIHZvdGVz,ots-29,otc-FFFFFF,oy-612,ox-70/et00062705-demctcsgsd-portrait.jpg' className='card-image rounded'/>
                <h5>83</h5>
                <p>Drama/Sports</p>
              </div>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:oi-discovery-catalog@@icons@@heart_202006300400.png,ox-24,oy-617,ow-29:ote-NjglICAzMDUgdm90ZXM%3D,ots-29,otc-FFFFFF,oy-612,ox-70/et00321654-pfnzsswwkp-portrait.jpg' className='card-image rounded'/>
                <h5>Scream (2022)</h5>
                <p>Horror/Thriller</p>
              </div>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:oi-discovery-catalog@@icons@@heart_202006300400.png,ox-24,oy-617,ow-29:ote-OTElICA0MjdrIHZvdGVz,ots-29,otc-FFFFFF,oy-612,ox-70/et00310790-cnlgffwsxt-portrait.jpg' className='card-image rounded'/>
                <h5>SpiderMan No-Way Home</h5>
                <p>Action/Adventure</p>
              </div>
            </div>
        </div>
    )
  }
}

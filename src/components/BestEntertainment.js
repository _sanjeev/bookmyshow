import './bestentertainment.css';
import React, { Component } from 'react';

export default class BestEntertainment extends Component {
  render() {
    return (
      <div className='container'>
        <div className='row mt-5'>
          <div className='col-12'>
            <h4 className='text-start'>The Best of Entertainment</h4>
          </div>
        </div>
        <div className='row d-flex justify-content-between'>
          <div className='col-2'>
            <img src='https://in.bmscdn.com/discovery-catalog/collections/tr:w-800,h-800:ote-MTM1KyBFdmVudHM%3D,otc-FFFFFF,otf-Roboto,ots-64,ox-48,oy-320,ott-b:w-300/workshops-collection-202007231330.png' className='entertainment-image rounded' />
          </div>
          <div className='col-2'>
            <img src='https://in.bmscdn.com/discovery-catalog/collections/tr:w-800,h-800:ote-MTArIEV2ZW50cw%3D%3D,otc-FFFFFF,otf-Roboto,ots-64,ox-48,oy-320,ott-b:w-300/fitness-collection-2020081150.png' className='entertainment-image rounded' />
          </div>
          <div className='col-2'>
            <img src='https://in.bmscdn.com/discovery-catalog/collections/tr:w-800,h-800:ote-MjUrIEV2ZW50cw%3D%3D,otc-FFFFFF,otf-Roboto,ots-64,ox-48,oy-320,ott-b:w-300/kids-collection-202007220710.png' className='entertainment-image rounded' />
          </div>
          <div className='col-2'>
            <img src='https://in.bmscdn.com/discovery-catalog/collections/tr:w-800,h-800:ote-OTArIEV2ZW50cw%3D%3D,otc-FFFFFF,otf-Roboto,ots-64,ox-48,oy-320,ott-b:w-300/comedy-shows-collection-202007220710.png' className='entertainment-image rounded' />
          </div>
          <div className='col-2'>
            <img src='https://in.bmscdn.com/discovery-catalog/collections/tr:w-800,h-800:ote-MTArIEV2ZW50cw%3D%3D,otc-FFFFFF,otf-Roboto,ots-64,ox-48,oy-320,ott-b:w-300/music-shows-collection-202007220710.png' className='entertainment-image rounded' />
          </div>
        </div>
      </div>
    )
  }
}

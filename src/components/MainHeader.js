import React, { Component } from 'react';
import Pic1 from './../images/download.jpeg';
// import './mainHeader.css';

export default class MainHeader extends Component {
  render() {
    return (
      <div class="pt-2 pb-5 container-fluid">
        <div id="carouselExampleControlsNoTouching" class="carousel slide w-100" data-bs-touch="false" data-bs-interval="false">
          <div class="carousel-inner w-100">
            <div class="carousel-item active w-100">
              <div className="d-flex  justify-content-evenly w-100">
                <div className="bg-secondary photo d-block w-100">
                  <img src="https://in.bmscdn.com/promotions/cms/creatives/1643109482063_freeweb.jpg" className='w-100' />
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div className="d-flex  justify-content-evenly ">
                <div className="bg-secondary photo d-block w-100">
                  <img src="https://in.bmscdn.com/promotions/cms/creatives/1641894791882_webbas.jpg" className='w-100' />
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div className="d-flex  justify-content-evenly ">
                <div className="bg-secondary photo d-block w-100">
                  <img src='https://in.bmscdn.com/promotions/cms/creatives/1642505477206_12x4.jpg' className='w-100' />
                </div>
              </div>
            </div>
          </div>
          <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControlsNoTouching" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" ></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControlsNoTouching" data-bs-slide="next">
            <span class="carousel-control-next-icon" ></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
      </div>
    );
  }
}

import './eventhappening.css';
import React, { Component } from 'react';

export default class EventHappening extends Component {
  render() {
    return (
        <div className='container'>
            <div className='row mt-5'>
              <div className='col-12'>
                <h4 className='text-start'>Event Happening Near You</h4>
              </div>
            </div>
            <div className='row d-flex justify-content-between'>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/nmcms/events/banner/desktop/media-desktop-potrait-sketching-0-2021-8-13-t-13-19-58.jpg' className='event-image rounded'/>
                <h5>Portrait Sketching-Hobbystation</h5>
                <p>Watch on Zoom</p>
                <p>0 kms away</p>
              </div>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/nmcms/events/banner/desktop/media-desktop-oriole-entertainment-live-0-2021-10-18-t-12-47-38.jpg' className='event-image rounded'/>
                <h5>Oriole Entertainment Live</h5>
                <p>Watch on Zoom</p>
                <p>0 kms away</p>
              </div>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/nmcms/events/banner/desktop/media-desktop-glitterz-kk-live-in-concert-0-2021-12-27-t-3-15-34.jpg' className='event-image rounded'/>
                <h5>GLITTERZ - KK Live in Concert</h5>
                <p>Dublin Square, Phoenix</p>
                <p>1.6 kms away</p>
              </div>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:ote-U3VuLCAzMCBKYW4%3D,ots-29,otc-FFFFFF,oy-612,ox-24/et00319590-ynujfrzyjj-portrait.jpg' className='event-image rounded'/>
                <h5>Sorabh Pant is Alive</h5>
                <p>The Huddle Mumbai</p>
                <p>3.7 kms away</p>
              </div>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:ote-U3VuLCAzMCBKYW4%3D,ots-29,otc-FFFFFF,oy-612,ox-24/et00321761-hpckxkwdld-portrait.jpg' className='event-image rounded'/>
                <h5>Rohan Gujral Live - A Stand up Comedy</h5>
                <p>The Huddle Mumbai</p>
                <p>3.7 kms away</p>
              </div>
            </div>
        </div>   
    )
  }
}

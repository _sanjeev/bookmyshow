import React, { Component } from 'react';
import BestEntertainment from './BestEntertainment';
import EventHappening from './EventHappening';
import Header from './Header';
import MainHeader from './MainHeader';
import Navbar from './Navbar';
import OnlineStreaming from './OnlineStreaming';
import OutdoorEvents from './OutdoorEvents';
import Recommended from './Recommended';
import TrendingSearch from './TrendingSearch';
import Main from './Main';
import Footer from './Footer';


export default class Home extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <Header />
                <MainHeader />
                <Main />
                <BestEntertainment />
                <EventHappening />
                <Recommended />
                <OnlineStreaming />
                <OutdoorEvents />
                <TrendingSearch />
                <OnlineStreaming />
                <Footer />
            </div>
        )
    }
}

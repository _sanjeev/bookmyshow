import React, { Component } from 'react';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import allMovies from '../Ticket';
import './seatbooking.css';

export default class SeatBooking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            seatSelected: []
        }
    }
    getShowDetail = () => {
        let showid = parseInt(this.props.match.params.id) % 10;
        let index = Math.floor(this.props.match.params.id / 10);
        let theaterName = allMovies[0].details[showid - 1].theaterName;
        let time = allMovies[0].details[showid - 1].showTime[index];
    }
    selectedSeat = (id) => {
        let { seatSelected } = { ...this.state };
        if (seatSelected.length < 10) {
            if (seatSelected.includes(id) === false) {


                seatSelected.push(id);


            } else {
                seatSelected = seatSelected.filter((element) => {
                    return element != id;
                })


            }
            this.setState({ seatSelected });

        }
    }


    render() {
        //   console.log("index",Math.floor(this.props.match.params.id/10));
        //   console.log("showid",this.props.match.params.id%10);
        //   let showid=parseInt(this.props.match.params.id)%10;
        //   let index=Math.floor(this.props.match.params.id/10);
        //   console.log("All Movies", allMovies[0].details[showid - 1].theaterName);
        //   console.log("time", allMovies[0].details[showid-1].showTime[index]);

        let showid = parseInt(this.props.match.params.id) % 10;
        let index = Math.floor(this.props.match.params.id / 10);
        let theaterName = allMovies[0].details[showid - 1].theaterName;
        let time = allMovies[0].details[showid - 1].showTime[index];
        return <div className='bg-white'>
            <div className='container-fluid '>
                <div className='row bg-blue'>
                    <div className='col-11 text-start text-white py-4'>
                        <span className='border-end pe-2'>{theaterName}</span><span className='ps-2'>Today,29 Jan, {time}</span>

                    </div>
                    <div className='col-1 d-flex align-items-center justify-content-end'>
                        {this.state.seatSelected.length > 0 ? <div className='text-white border border-white rounded px-3 py-2'>{this.state.seatSelected.length} Ticket</div> : null}

                    </div>

                </div>
                <div className='row seats my-5 border-top'>
                    <div className='container w-50'>
                        <div className='row'>
                            <div className='col-8 d-flex justify-content-between align-items-center'>
                                <div className='px-4'>A</div>
                                <div className='d-flex justify-content-between'>
                                    <div onClick={() => { this.selectedSeat('1A') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("1A") ? "selected" : ""}`}>1</div></div>
                                    <div onClick={() => { this.selectedSeat('2A') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("2A") ? "selected" : ""}`}>2</div></div>
                                    <div onClick={() => { this.selectedSeat('3A') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("3A") ? "selected" : ""}`}>3</div></div>
                                    <div onClick={() => { this.selectedSeat('4A') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("4A") ? "selected" : ""}`}>4</div></div>
                                    <div onClick={() => { this.selectedSeat('5A') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("5A") ? "selected" : ""}`}>5</div></div>
                                    <div onClick={() => { this.selectedSeat('6A') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("6A") ? "selected" : ""}`}>6</div></div>

                                    <div onClick={() => { this.selectedSeat('7A') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("7A") ? "selected" : ""}`}>7</div></div>
                                    <div onClick={() => { this.selectedSeat('8A') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("8A") ? "selected" : ""}`}>8</div></div>
                                    <div onClick={() => { this.selectedSeat('9A') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("9A") ? "selected" : ""}`}>9</div></div>
                                    <div onClick={() => { this.selectedSeat('10A') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("10A") ? "selected" : ""}`}>10</div></div>
                                </div>








                            </div>
                        </div>
                        <div className='row'>
                            <div className='col-8 d-flex justify-content-between align-items-center'>
                                <div className='px-4'>B</div>
                                <div className='d-flex justify-content-between'>
                                    <div onClick={() => { this.selectedSeat('1B') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("1B") ? "selected" : ""}`}>1</div></div>
                                    <div onClick={() => { this.selectedSeat('2B') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("2B") ? "selected" : ""}`}>2</div></div>
                                    <div onClick={() => { this.selectedSeat('3B') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("3B") ? "selected" : ""}`}>3</div></div>
                                    <div onClick={() => { this.selectedSeat('4B') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("4B") ? "selected" : ""}`}>4</div></div>
                                    <div onClick={() => { this.selectedSeat('5B') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("5B") ? "selected" : ""}`}>5</div></div>
                                    <div onClick={() => { this.selectedSeat('6B') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("6B") ? "selected" : ""}`}>6</div></div>

                                    <div onClick={() => { this.selectedSeat('7B') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("7B") ? "selected" : ""}`}>7</div></div>
                                    <div onClick={() => { this.selectedSeat('8B') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("8B") ? "selected" : ""}`}>8</div></div>
                                    <div onClick={() => { this.selectedSeat('9B') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("9B") ? "selected" : ""}`}>9</div></div>
                                    <div onClick={() => { this.selectedSeat('10B') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("10B") ? "selected" : ""}`}>10</div></div>
                                </div>
                            </div>

                        </div>

                        <div className='row'>
                            <div className='col-8 d-flex justify-content-between align-items-center'>
                                <div className='px-4'>C</div>
                                <div className='d-flex justify-content-between'>
                                    <div onClick={() => { this.selectedSeat('1C') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("1C") ? "selected" : ""}`}>1</div></div>
                                    <div onClick={() => { this.selectedSeat('2C') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("2C") ? "selected" : ""}`}>2</div></div>
                                    <div onClick={() => { this.selectedSeat('3C') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("3C") ? "selected" : ""}`}>3</div></div>
                                    <div onClick={() => { this.selectedSeat('4C') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("4C") ? "selected" : ""}`}>4</div></div>
                                    <div onClick={() => { this.selectedSeat('5C') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("5C") ? "selected" : ""}`}>5</div></div>
                                    <div onClick={() => { this.selectedSeat('6C') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("6C") ? "selected" : ""}`}>6</div></div>

                                    <div onClick={() => { this.selectedSeat('7C') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("7C") ? "selected" : ""}`}>7</div></div>
                                    <div onClick={() => { this.selectedSeat('8C') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("8C") ? "selected" : ""}`}>8</div></div>
                                    <div onClick={() => { this.selectedSeat('9C') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("9C") ? "selected" : ""}`}>9</div></div>
                                    <div onClick={() => { this.selectedSeat('10C') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("10C") ? "selected" : ""}`}>10</div></div>
                                </div>
                            </div>

                        </div>

                        <div className='row'>
                            <div className='col-8 d-flex justify-content-between align-items-center'>
                                <div className='px-4'>D</div>
                                <div className='d-flex justify-content-between'>
                                    <div onClick={() => { this.selectedSeat('1D') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("1D") ? "selected" : ""}`}>1</div></div>
                                    <div onClick={() => { this.selectedSeat('2D') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("2D") ? "selected" : ""}`}>2</div></div>
                                    <div onClick={() => { this.selectedSeat('3D') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("3D") ? "selected" : ""}`}>3</div></div>
                                    <div onClick={() => { this.selectedSeat('4D') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("4D") ? "selected" : ""}`}>4</div></div>
                                    <div onClick={() => { this.selectedSeat('5D') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("5D") ? "selected" : ""}`}>5</div></div>
                                    <div onClick={() => { this.selectedSeat('6D') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("6D") ? "selected" : ""}`}>6</div></div>

                                    <div onClick={() => { this.selectedSeat('7D') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("7D") ? "selected" : ""}`}>7</div></div>
                                    <div onClick={() => { this.selectedSeat('8D') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("8D") ? "selected" : ""}`}>8</div></div>
                                    <div onClick={() => { this.selectedSeat('9D') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("9D") ? "selected" : ""}`}>9</div></div>
                                    <div onClick={() => { this.selectedSeat('10D') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("10D") ? "selected" : ""}`}>10</div></div>
                                </div>
                            </div>

                        </div>

                        <div className='row'>
                            <div className='col-8 d-flex justify-content-between align-items-center'>
                                <div className='px-4'>E</div>
                                <div className='d-flex justify-content-between'>
                                    <div onClick={() => { this.selectedSeat('1E') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("1E") ? "selected" : ""}`}>1</div></div>
                                    <div onClick={() => { this.selectedSeat('2E') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("2E") ? "selected" : ""}`}>2</div></div>
                                    <div onClick={() => { this.selectedSeat('3E') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("3E") ? "selected" : ""}`}>3</div></div>
                                    <div onClick={() => { this.selectedSeat('4E') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("4E") ? "selected" : ""}`}>4</div></div>
                                    <div onClick={() => { this.selectedSeat('5E') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("5E") ? "selected" : ""}`}>5</div></div>
                                    <div onClick={() => { this.selectedSeat('6E') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("6E") ? "selected" : ""}`}>6</div></div>

                                    <div onClick={() => { this.selectedSeat('7E') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("7E") ? "selected" : ""}`}>7</div></div>
                                    <div onClick={() => { this.selectedSeat('8E') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("8E") ? "selected" : ""}`}>8</div></div>
                                    <div onClick={() => { this.selectedSeat('9E') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("9E") ? "selected" : ""}`}>9</div></div>
                                    <div onClick={() => { this.selectedSeat('10E') }} className='px-2 py-3'><div className={` outer-div d-flex align-items-center justify-content-center ${this.state.seatSelected.includes("10E") ? "selected" : ""}`}>10</div></div>
                                </div>
                            </div>

                        </div>


                    </div>
                    {
                        this.state.seatSelected.length > 0 ?
                            <div className='container bg-dark'>
                                <div className='row d-flex justify-content-center'>
                                    <div className='col-3 py-3'>
                                        <Link to="/successfullybookticket"> <button className='px-5 py-2 border-0 rounded w-75'>Pay Rs {Number(this.state.seatSelected.length) * 150}</button>
                                        </Link>
                                        </div>

                                </div>

                            </div> : ""
                    }




                </div>

            </div>

        </div>;
    }
}

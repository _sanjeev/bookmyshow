import React, { Component } from 'react';
import './main.css';
import res from '../data.js';
import { Link } from 'react-router-dom';

export default class main extends Component {
    render() {
        console.log(res);
        return (
            <div className='container'>
                <div className='row mt-5'>
                    <div className='col-12'>
                        <div className='d-flex justify-content-between'>
                            <div>
                                <h4 className='text-start'>Recommended Movies</h4>
                            </div>
                            <div>
                                <Link to={{pathname: "movies"}} className='text-decoration-none text-danger'>
                                    <p>See All</p>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row d-flex justify-content-between'>
                    {res.map((key) => (
                        <div className='col-2'>
                            <Link to={{ pathname: `/details/${key.id}` }} className='text-decoration-none text-black'>
                                <img src={key.image} className='card-image rounded' />
                                <h5>{key.movie_name}</h5>
                                <p>{key.movie_type}</p>
                            </Link>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

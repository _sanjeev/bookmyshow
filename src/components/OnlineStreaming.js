import './onlinestreaming.css';
import React, { Component } from 'react';

export default class OnlineStreaming extends Component {
  render() {
    return (
        <div className='container'>
            <div className='row mt-5'>
              <div className='col-12'>
                <h4 className='text-start'>Online Streaming Events</h4>
              </div>
            </div>
            <div className='row d-flex justify-content-between'>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:ote-VGh1LCAyNyBKYW4gb253YXJkcw%3D%3D,ots-29,otc-FFFFFF,oy-612,ox-24/et00313611-bnvfftrcnu-portrait.jpg' className='card-image rounded'/>
                <h5>Zombivli</h5>
                <p>Commedy/Horror</p>
              </div>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:ote-U2F0LCAyOSBKYW4%3D,ots-29,otc-FFFFFF,oy-612,ox-24/et00321307-guphvphlty-portrait.jpg' className='card-image rounded'/>
                <h5>Pushpa: The Rise</h5>
                <p>Action/Thriller</p>
              </div>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:ote-U3VuLCAzMCBKYW4%3D,ots-29,otc-FFFFFF,oy-612,ox-24/et00317481-abqdjqusjp-portrait.jpg' className='card-image rounded'/>
                <h5>83</h5>
                <p>Drama/Sports</p>
              </div>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:ote-U2F0LCA1IEZlYg%3D%3D,ots-29,otc-FFFFFF,oy-612,ox-24/et00145818-hbakuphvea-portrait.jpg' className='card-image rounded'/>
                <h5>Scream (2022)</h5>
                <p>Horror/Thriller</p>
              </div>
              <div className='col-2'>
                <img src='https://in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:ote-U3VuLCAzMCBKYW4%3D,ots-29,otc-FFFFFF,oy-612,ox-24/et00321365-atupumuhsv-portrait.jpg' className='card-image rounded'/>
                <h5>SpiderMan No-Way Home</h5>
                <p>Action/Adventure</p>
              </div>
            </div>
        </div>
    )
  }
}

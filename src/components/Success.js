import React, { Component } from 'react';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import Navbar from './Navbar';

export default class Success extends Component {
  render() {
    return <div className='bg-white'>
        <Navbar />
        <h1 className='text-success'>SuccessFully Book The Ticket</h1>
        <Link  to="/" className="text-decoration-none text-dark">
        Go To Home
        </Link>
    </div>;
  }
}

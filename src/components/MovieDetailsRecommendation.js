import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './MovieDetailsRecommendation.css';
export default class MovieDetailsRecommendation extends Component {
    render() {
        const { data } = this.props;
        console.log(data);
        return (
            <div className='container'>
                <div className='row mt-5'>
                    <div className='col-12'>
                        <h4 className='text-start'>You Might Also Like This</h4>
                    </div>
                </div>
                <div className='row d-flex justify-content-start'>
                    {data.map((key) => (
                        <div className='col-3'>
                            <Link to={{ pathname: `/details/${key.id}` }} className='text-decoration-none text-black'>
                                <img src={key.image} className='recommended-image rounded' />
                                <h5>{key.movie_name}</h5>
                                <p>{key.movie_type}</p>
                            </Link>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}
